import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

export const getVendedores = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/vendedores`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getVendedoresDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/vendedores/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getVendedor = async (id) => {
    try {
        const response = await api.get(`/usuario/vendedor/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getVendedoresCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/vendedores/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'vendedores.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteVendedor = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/vendedor/${params.id}`, {});
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertVendedor = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/vendedor`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateVendedor = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/vendedor/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error en el api de update:', error);
        throw error;
    }
};

export const getMaxVendedorId = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/vendedor/maxid`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
