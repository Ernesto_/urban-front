import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getOrdenesTrabajo = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/trabajos`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getOrdenTrabajo = async (id) => {
    try {
        const response = await api.get(`/usuario/trabajo/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertOrdenTrabajo = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/trabajo`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getTrabajosDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/trabajos/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteOrdenTrabajo = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/trabajo/${params.id}`);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateOrdenTrabajo = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/trabajo/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error en el api de update:', error);
        throw error;
    }
};

export const getMaxOrdenTrabajoId = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/trabajo/maxid`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
