import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getAsiento = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/contabilidad/asiento`, {
            params: { anio: params.anio, empresa: params.empresa, asiento: params.asiento }
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getCuentasContables = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/contabilidad/cuentascontables`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateAsiento = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/asiento/update`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
