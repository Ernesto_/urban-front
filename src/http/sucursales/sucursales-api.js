import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

export const getSucursales = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/definiciones/generales/sucursales`, {
            params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getSucursal = async (id) => {
    try {
        const response = await api.get(`/usuario/definiciones/generales/sucursal/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
 