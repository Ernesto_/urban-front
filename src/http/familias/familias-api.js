import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getFamilias = async (params = {  }) => {
    try {
        const response = await api.get(`/usuario/familias`, {
            params: params
        });
         return response.data;
        //return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getFamiliasDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/familias/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getProductosByFamilia = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/familia/detalle`, {
            params: params
        });
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getFamilia = async (id) => {
    try {
        const response = await api.get(`/usuario/familia/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getProductosFamilia = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/familia/productos/${params.id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertFamilia = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/familia`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteFamilia = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/familia/${params.id}`);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateFamilia = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/familia/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error en el api de update:', error);
        throw error;
    }
};

export const getFamiliasCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/familias/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'familias.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getMaxFamiliaId = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/familia/maxid`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
