import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

export const getCuentasBancariasDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/bancos/cuentas/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
