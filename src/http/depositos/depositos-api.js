import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getDepositos = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/depositos`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getDepositosDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/depositos/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
 

export const getDeposito = async (id) => {
    try {
        const response = await api.get(`/usuario/deposito/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

 
export const insertDeposito = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/deposito`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteDeposito = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/deposito/${params.id}`);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateDeposito = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/deposito/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error en el api de update:', error);
        throw error;
    }
};

export const getDepositosCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/depositos/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'depositos.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getMaxDepositoId = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/deposito/maxid`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
