import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getOperaciones = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/operaciones`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getOperacion = async (id) => {
    try {
        const response = await api.get(`/usuario/operacion/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getTiposOperaciones = async () => {
    try {
        const response = await api.get(`/usuario/operaciones/tipos`);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getFormas = async () => {
    try {
        const response = await api.get(`/usuario/operaciones/formas/dropdown`);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertOperacion = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/operacion`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteOperacion = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/operacion/${params.id}`);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateOperacion = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/operacion/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error en el api de update:', error);
        throw error;
    }
};

export const getMaxOperacionId = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/operacion/maxid`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
