import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getMaquinas = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/maquinas`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getMaquinasDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/maquinas/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getMaquina = async (id) => {
    try {
        const response = await api.get(`/usuario/maquina/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertMaquina = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/maquina`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteMaquina = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/maquina/${params.id}`);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateMaquina = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/maquina/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getMaxMaquinaId = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/maquina/maxid`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
