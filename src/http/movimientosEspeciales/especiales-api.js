import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

export const getEspeciales = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/especiales/index`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getTiposEspeciales = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/tipos/especiales/index`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getEspecial = async (id) => {
    try {
        const response = await api.get(`/usuario/especial/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getTipoEspecial = async (id) => {
    try {
        const response = await api.get(`/usuario/tipo/especial/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getTiposMovimientosDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/especiales/tipos/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getEspecialesDetalles = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/especiales/detalle`, {
            params: params
        });
        //return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
