import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getTrabajoEspecificacion = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/trabajoespecificacion`, {
            params: { trabajo: params.trabajo, item: params.item, tipo: params.tipo }
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertTrabajoEspecificacion = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/trabajoespecificacion`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteTrabajoEspecificacion = async (params = {}) => {
    try {
        const { id, id2, id3 } = params; // Desestructura los parámetros adicionales

        // Modifica la URL de la solicitud DELETE para incluir los parámetros adicionales
        const response = await api.delete(`/usuario/trabajoespecificacion/${id}/${id2}/${id3}`);

        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateTrabajoEspecificacion = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/trabajoespecificacion`, params);
        return response;
    } catch (error) {
        console.error('Error en el api de update:', error);
        throw error;
    }
};
