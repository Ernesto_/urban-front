import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getEstadoCuenta = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/proveedores/saldo`, {
            // params: { id: params.id }
            params: params
        });
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getProveedores = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/proveedores`, {
            params: params
        });
        return response.data;
        debugger;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getGruposProveedores = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/proveedores/grupos`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getProveedoresDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/proveedores/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getProveedor = async (id) => {
    try {
        const response = await api.get(`/proveedor/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getProveedoresCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/proveedores/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'proveedores.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteProveedor = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/proveedor/${params.id}`);
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertProveedor = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/proveedor`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateProveedor = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/proveedor/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error en el api de update:', error);
        throw error;
    }
};

export const getMaxProveedorId = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/proveedor/maxid`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
