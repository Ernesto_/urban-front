import api from '../../api';

export const getResumenVentas = async (params = {}, filter = null) => {
    try {
        const response = await api.get(`/usuario/ventas/mensuales`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
