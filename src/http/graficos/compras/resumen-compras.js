import api from '../../api';

export const getResumenCompras = async (params = {}, filter = null) => {
    try {
        const response = await api.get(`/usuario/compras/mensuales`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
