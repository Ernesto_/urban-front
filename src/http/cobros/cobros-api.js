import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getCobros = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/cobros`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getFormasPagoDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/cobros/formaspago/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};


export const getCobro = async (id) => {
    try {
        const response = await api.get(`/usuario/cobro/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
 
export const getCobrosDetalle = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/cobros/detalle`, {
            params: params
        });
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getCobrosFactura = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/cobros/factura`, {
            params: { factura: params.factura, tipo: params.tipo, sucursal: params.sucursal }
        });
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
