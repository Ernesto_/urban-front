import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

export const getTimbrados = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/timbrados`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getTimbrado = async (id) => {
    try {
        const response = await api.get(`/usuario/timbrado/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getTimbradosCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/timbrados/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'timbrados.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteTimbrado = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/timbrado/${params.id}`, {});
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertTimbrado = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/timbrado`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateTimbrado = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/timbrado/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
