import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getOperadores = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/operadores`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getOperadoresDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/operadores/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getOperador = async (id) => {
    try {
        const response = await api.get(`/usuario/operador/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertOperador = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/operador`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteOperador = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/operador/${params.id}`, {});
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateOperador = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/operador/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getMaxOperadorId = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/operador/maxid`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
