import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

export const getUnidades = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/unidades`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getUnidad = async (id) => {
    try {
        const response = await api.get(`/usuario/unidad/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getUnidadesCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/unidades/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'unidadesmedida.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteUnidad = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/unidad/${params.id}`);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertUnidad = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/unidad`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateUnidad = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/unidad/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getMaxUnidadId = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/unidad/maxid`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
