import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

export const getAnticipos = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/anticipos`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getAnticiposPDF = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/anticipos/pdf`, {
            params: params
        });
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getClientesDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/clientes/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getAnticipo = async (id) => {
    try {
        const response = await api.get(`/usuario/anticipo/${id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getTiposDocumentos = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/clientes/tipodocumentos`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
 

export const getEstadoCuenta = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/clientes/saldo`, {
            // params: { id: params.id }
            params: params
        });
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getClientesCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/clientes/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'clientes.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const deleteAnticipo = async (params = {}) => {
    try {
        const response = await api.delete(`/usuario/anticipo/${params.id}`, {});
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const insertAnticipo = async (params = {}) => {
    try {
        const response = await api.post(`/usuario/anticipo`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const updateAnticipo = async (params = {}) => {
    try {
        const response = await api.put(`/usuario/anticipo/${params.id}`, params);
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getMaxId = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/anticipo/maxid`, params);
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
