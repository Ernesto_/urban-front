import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

export const getIvaVentas = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/iva/ventas`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getIvaVentasPDF = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/iva/ventas/pdf`, {
            params: params
        });
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getIvaVentasCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/iva/ventas/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'libroIvaVentas.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getIvaCompras = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/iva/compras`, {
            params: params
        });
       // return response.data;
       return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getIvaComprasCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/iva/compras/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'libroIvaCompras.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
