import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

export const getOrdenesPagos = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/ordenespagos/index`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getOrdenPago = async (id) => {
    try {
        const response = await api.get(`/usuario/ordenpago/${id}`, {});

        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
