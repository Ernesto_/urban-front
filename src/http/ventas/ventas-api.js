import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getVentas = async (params = { pagina: 1, search: '' }) => {
    try {
        const response = await api.get(`/usuario/ventas`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getVentasPDF = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/ventas/pdf`, {
            params: params
        });
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getPlanPagoVenta = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/venta/planpago`, {
            params: { factura: params.factura, tipo: params.tipo, sucursal: params.sucursal }
        });
        //return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getVenta = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/venta`, {
            params: { factura: params.factura, tipo: params.tipo, sucursal: params.sucursal }
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getCondicionesVentaDropdown = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/ventas/condiciones/dropdown`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getVentasFamilias = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/ventas/resumen/familias`, {
            params: params
        });
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getResumenVentasFamiliasMeses = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/ventas/resumen/familias/meses`, {
            params: params
        });
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getVentasDetallesPDF = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/ventas/detalle/pdf`, {
            params: params
        });
        //return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};


export const getRankingVentasProductos = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/ventas/ranking/productos`, {
            params: params
        });
        //return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getRankingVentasClientes = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/ventas/ranking/clientes`, {
            params: params
        });
        //return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};


export const getVentasCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/ventas/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'ventas.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getAsientoVenta = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/contabilidad/asiento`, {
            params: { ejercicio: params.ejercicio, empresa: params.empresa, numero: params.numero }
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
