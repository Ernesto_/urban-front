import api from '../api';

//export const csrfCookie = () => api.get('/sanctum/csrf-cookie');

api.setAcceptHeader('json');

export const getCompras = async (params = { pagina: 1, fecha_desde: null, fecha_hasta: null }) => {
    try {
        const response = await api.get(`/usuario/compras`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getPlanPagoCompra = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/compra/planpago`, {
            params: { id: params.id }
        });
        //return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};


export const getResumenComprasFamiliasMeses = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/compras/resumen/familias/meses`, {
            params: params
        });
        // return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};


export const getCompra = async (params = {}) => {
    try {
        const response = await api.get(`/usuario/compra/${params.id}`, {});
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getComprasCSV = async (params = { search: '' }) => {
    try {
        const response = await api.get(`/usuario/compras/csv`, {
            responseType: 'blob',
            params: params
        });
        const url = URL.createObjectURL(response.data);
        const a = document.createElement('a');
        a.href = url;
        a.download = 'compras.csv';
        a.click();
        URL.revokeObjectURL(url);
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getComprasDetalles = async (params = { pagina: 1, fecha_desde: null, fecha_hasta: null }) => {
    try {
        const response = await api.get(`/usuario/compras/detalle`, {
            params: params
        });
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};

export const getComprasDetallesPDF = async (params = {  }) => {
    try {
        const response = await api.get(`/usuario/compras/detalle/pdf`, {
            params: params
        });
        //return response.data;
        return response;
    } catch (error) {
        console.error('Error:', error);
        throw error;
    }
};
