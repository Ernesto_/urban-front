import { defineStore } from 'pinia';
import { computed, ref, reactive } from 'vue';
import { rucLogin, login } from '../http/auth-api';
import { getNombreEmpresa } from '../http/parametros/parametros-api';
import api from '../http/api';
import { getAttributesFromData } from '@/utils/transformers/fetchTransformers.js';


export const useAuthStore = defineStore('authStore', () => {
    const user = ref(null);
    const token = ref('');
    const loginErrorMessage = ref();
    const hasLoggedInWithRuc = ref(false);
    const dataBaseConfig = reactive({ direccion_ip: '', puerto: '', empresa: '' });
    const isLoggedIn = computed(() => !!user.value);

    const tryRucLogin = async (ruc) => {
        try {
            const { data } = await rucLogin(ruc);

            dataBaseConfig.direccion_ip = data.direccion_ip;
            dataBaseConfig.puerto = data.puerto;
            dataBaseConfig.entorno = data.entorno.toUpperCase();
           // dataBaseConfig.empresa = data.entorno.toUpperCase();
0
            hasLoggedInWithRuc.value = true;

            return data;
        } catch (error) {
            hasLoggedInWithRuc.value = false;
            if (error.response && (error.response.status === 422 || error.response.status === 401 || error.response.status === 403)) {
                loginErrorMessage.value = error.response.data.error;
            }
        }
    };

    const tryLogin = async (credentials) => {
        try {
            const { data } = await login(credentials);
 
            console.log("a la chingaa")
            
            user.value = credentials.user.toUpperCase();
            token.value = data.token;

            api.defaults.headers.common['access-token'] = token.value;

            const response = await getNombreEmpresa();

           const {nombre} = getAttributesFromData(response);
 
            dataBaseConfig.empresa = nombre;
 
            //loginErrorMessage.value = null;
        } catch (error) {
            if (error.response && (error.response.status === 422 || error.response.status === 401 || error.response.status === 403)) {
                debugger;
                // loginErrorMessage.value = error.response.data.error;
                loginErrorMessage.value = error.response.data.message;
            }
        }
    };

    const handleLogout = async () => {
        user.value = null;
        token.value = '';
        api.defaults.headers.common['access-token'] = null;
    };

    return {
        user,
        isLoggedIn,
        loginErrorMessage,
        tryLogin,
        tryRucLogin,
        handleLogout,
        hasLoggedInWithRuc,
        dataBaseConfig,
        token
    };
});
