import { defineStore } from 'pinia';
import { getVendedoresDropdown } from '@/http/vendedores/vendedores-api';
import { ref } from 'vue';

export const useVendedoresStore = defineStore('Vendedores', () => {
    const vendedores = ref([]);

    async function fetchVendedores() {
        try {
            const vendedoresIndex = await getVendedoresDropdown();
            vendedores.value = vendedoresIndex;
        } catch (error) {
            console.error('Error fetching de vendedores:', error);
            throw error;
        }
    }

    return { vendedores, fetchVendedores };
});
