import { defineStore } from 'pinia';
import { getFamiliasDropdown } from '@/http/familias/familias-api';
import { ref } from 'vue';

export const useFamiliasStore = defineStore('Familias', () => {
    const familias = ref([]);

    async function fetchFamilias() {
        try {
            const familiasIndex = await getFamiliasDropdown();
            familias.value = familiasIndex;
        } catch (error) {
            console.error('Error fetching familias:', error);
            throw error;
        }
    }

    return { familias, fetchFamilias };
});
