import { defineStore } from 'pinia';
import { getGruposProveedores, getProveedoresDropdown } from '@/http/proveedores/proveedores-api';
import { ref } from 'vue';

export const useProveedoresStore = defineStore('Proveedores', () => {
    const proveedores = ref([]);
    const gruposProveedores = ref([]);

    async function fetchProveedores() {
        try {
            const proveedoresIndex = await getProveedoresDropdown();
            proveedores.value = proveedoresIndex;
        } catch (error) {
            console.error('Error fetching de proveedores:', error);
            throw error;
        }
    }

    async function fetchGruposProveedores() {
        try {
            // Realiza la lógica para obtener las cuentas contables
            const grupos = await getGruposProveedores(); // Aquí realiza la llamada al API
            gruposProveedores.value = grupos; // Usa .value para asignar el nuevo valor
        } catch (error) {
            console.error('Error fetching grupos de proveedores:', error);
            throw error;
        }
    }

    return { proveedores, gruposProveedores, fetchProveedores, fetchGruposProveedores };
});
