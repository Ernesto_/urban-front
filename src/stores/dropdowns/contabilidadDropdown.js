import { defineStore } from 'pinia';
import { getCuentasContables } from '@/http/contabilidad/contabilidad-api';
import { ref } from 'vue';

export const useCuentasContablesStore = defineStore('cuentasContables', () => {
    const cuentasContables = ref([]);
    async function fetchCuentasContables() {
        try {
            // Realiza la lógica para obtener las cuentas contables
            const cuentas = await getCuentasContables(); // Aquí realiza la llamada al API
            cuentasContables.value = cuentas; // Usa .value para asignar el nuevo valor
        } catch (error) {
            console.error('Error fetching cuentas contables:', error);
            throw error;
        }
    }

    return { cuentasContables, fetchCuentasContables };
});
