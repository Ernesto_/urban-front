import AppLayout from '@/layout/AppLayout.vue';

const routes = [
    {
        path: '/',
        component: AppLayout,
        meta: {
            auth: true
        },
        children: [
            {
                path: '/',
                name: 'dashboard',
                component: () => import('@/views/Dashboard.vue')
            },
            {
                path: '/sucursales',
                name: 'sucursales',
                component: () => import('@/views/pages/sucursales/SucursalesIndex.vue')
            },
            {
                path: '/sucursal/:id',
                name: 'sucursal',
                component: () => import('@/views/pages/sucursales/SucursalShow.vue')
            },
            {
                path: '/tipos-especiales',
                name: 'tipos-especiales',
                component: () => import('@/views/pages/movimientosEspeciales/TipoEspecialesIndex.vue')
            },
            {
                path: '/tipo-especial/:id',
                name: 'tipo-especial',
                component: () => import('@/views/pages/movimientosEspeciales/TipoEspecialShow.vue')
            },
            {
                path: '/ordenes-pagos',
                name: 'ordenes-pagos',
                component: () => import('@/views/pages/ordenesPagos/OrdenesPagosIndex.vue')
            },
            {
                path: '/orden-pago/:id',
                name: 'orden-pago',
                component: () => import('@/views/pages/ordenesPagos/OrdenPagoShow.vue')
            },
            {
                path: '/especiales',
                name: 'especiales',
                component: () => import('@/views/pages/movimientosEspeciales/EspecialesIndex.vue')
            },
            {
                path: '/especial/:id',
                name: 'especial',
                component: () => import('@/views/pages/movimientosEspeciales/EspecialShow.vue')
            },

            {
                path: '/especiales-detalles',
                name: 'especiales-detalles',
                component: () => import('@/views/pages/movimientosEspeciales/detalle/EspecialesDetallesReportIndex.vue')
            },
            {
                path: '/proforma-movimiento-especial/:id',
                name: 'proforma-movimiento-especial',
                component: () => import('@/views/pages/movimientosEspeciales/Proforma.vue')
            },
            {
                path: '/anticipos',
                name: 'anticipos',
                component: () => import('@/views/pages/anticipos/AnticiposIndex.vue')
            },
            {
                path: '/anticipo/:id',
                name: 'anticipo',
                component: () => import('@/views/pages/anticipos/AnticipoShow.vue')
            },
            {
                path: '/anticipo',
                name: 'anticipo-insert',
                component: () => import('@/views/pages/anticipos/AnticipoShow.vue')
            },
            {
                path: '/usuarios',
                name: 'usuarios',
                component: () => import('@/views/pages/usuarios/UsuariosIndex.vue')
            },
            {
                path: '/clientes',
                name: 'clientes',
                component: () => import('@/views/pages/clientes/ClientesIndex.vue')
            },
            {
                path: '/cliente/:id',
                name: 'cliente',
                component: () => import('@/views/pages/clientes/ClienteShow.vue')
            },
            {
                path: '/cliente',
                name: 'cliente-insert',
                component: () => import('@/views/pages/clientes/ClienteShow.vue')
            },
            {
                path: '/cliente/ventas/ultimo/anio/:id',
                name: 'cliente-ventas-ultimo-anio',
                component: () => import('@/views/pages/graficos/clientes/ultimo_anio/VentasUltimoAnio.vue')
            },

            {
                path: '/proveedores',
                name: 'proveedores',
                component: () => import('@/views/pages/proveedores/ProveedoresIndex.vue')
            },
            {
                path: '/proveedor/:id',
                name: 'proveedor',
                component: () => import('@/views/pages/proveedores/ProveedorShow.vue')
            },
            {
                path: '/proveedor',
                name: 'proveedor-insert',
                component: () => import('@/views/pages/proveedores/ProveedorShow.vue')
            },
            {
                path: '/unidades-medida',
                name: 'unidades-medida',
                component: () => import('@/views/pages/medidas/MedidasIndex.vue')
            },
            {
                path: '/unidad-medida/:id',
                name: 'unidad-medida',
                component: () => import('@/views/pages/medidas/MedidaShow.vue')
            },
            {
                path: '/unidad-medida',
                name: 'unidad-medida-insert',
                component: () => import('@/views/pages/medidas/MedidaShow.vue')
            },
            {
                path: '/vendedores',
                name: 'vendedores',
                component: () => import('@/views/pages/vendedores/VendedoresIndex.vue')
            },
            {
                path: '/vendedor/:id',
                name: 'vendedor',
                component: () => import('@/views/pages/vendedores/VendedorShow.vue')
            },
            {
                path: '/vendedor',
                name: 'vendedor-insert',
                component: () => import('@/views/pages/vendedores/VendedorShow.vue')
            },
            {
                path: '/timbrados',
                name: 'timbrados',
                component: () => import('@/views/pages/timbrados/TimbradosIndex.vue')
            },
            {
                path: '/timbrado/:id',
                name: 'timbrado',
                component: () => import('@/views/pages/timbrados/TimbradoShow.vue')
            },
            {
                path: '/timbrado',
                name: 'timbrado-insert',
                component: () => import('@/views/pages/timbrados/TimbradoShow.vue')
            },
            {
                path: '/familias',
                name: 'familias',
                component: () => import('@/views/pages/familias/FamiliasIndex.vue')
            },
            {
                path: '/familia/:id',
                name: 'familia',
                component: () => import('@/views/pages/familias/FamiliaShow.vue')
            },
            {
                path: '/familia',
                name: 'familia-insert',
                component: () => import('@/views/pages/familias/FamiliaShow.vue')
            },
            {
                path: '/productos',
                name: 'productos',
                component: () => import('@/views/pages/productos/ProductosIndex.vue')
            },
            {
                path: '/producto/:id',
                name: 'producto',
                component: () => import('@/views/pages/productos/ProductoShow.vue')
            },
            {
                path: '/producto',
                name: 'producto-insert',
                component: () => import('@/views/pages/productos/ProductoShow.vue')
            },

            {
                path: '/productos-precios',
                name: 'productos-precios',
                component: () => import('@/views/pages/productos/precios/ProductosPreciosReportIndex.vue')
            },

            {
                path: '/productos-stock',
                name: 'productos-stock',
                component: () => import('@/views/pages/productos/stock/ProductosStock.vue')
            },

            {
                path: '/productos-familias',
                name: 'productos-familias',
                component: () => import('@/views/pages/familias/ProductosFamiliasReportIndex.vue')
            },
            {
                path: '/compras',
                name: 'compras',
                component: () => import('@/views/pages/compras/ComprasIndex.vue')
            },
            {
                path: '/compra/:id',
                name: 'compra',
                component: () => import('@/views/pages/compras/CompraShow.vue')
            },
            {
                path: 'plan-pago/compra/:id',
                name: 'plan-pago/compra',
                component: () => import('@/views/pages/compras/PlanPagoReportIndex.vue')
            },
            {
                path: '/compras-detalles',
                name: 'compras-detalles',
                component: () => import('@/views/pages/compras/detalle/ComprasDetallesReportIndex.vue')
            },
            {
                path: '/compras-meses-familias',
                name: 'compras-meses-familias',
                component: () => import('@/views/pages/compras/familias/ComprasMensualesFamiliasReportIndex.vue')
            },
            {
                path: '/libro-iva/compras',
                name: 'libro-iva/compras',
                component: () => import('@/views/pages/iva/compras/IvaComprasReportIndex.vue')
            },
            {
                path: '/ventas',
                name: 'ventas',
                component: () => import('@/views/pages/ventas/VentasIndex.vue')
            },
            {
                path: '/ventas-ranking-productos',
                name: 'ventas-ranking-productos',
                component: () => import('@/views/pages/ventas/ranking/RankingVentasProductosReportIndex.vue')
            },
            {
                path: '/ventas-ranking-clientes',
                name: 'ventas-ranking-clientes',
                component: () => import('@/views/pages/ventas/ranking/RankingVentasClientesReportIndex.vue')
            },
            {
                path: '/ventas/pdf',
                name: 'ventas/pdf',
                component: () => import('@/views/pages/ventas/VentasReportIndex.vue')
            },
            {
                path: '/anticipos/pdf',
                name: 'anticipos/pdf',
                component: () => import('@/views/pages/anticipos/AnticiposReportIndex.vue')
            },

            {
                path: '/libro-iva/ventas/pdf',
                name: '/libro-iva/ventas/pdf',
                component: () => import('@/views/pages/iva/ventas/IvaVentasReportIndex.vue')
            },

            {
                path: '/ventas-familias',
                name: 'ventas-familias',
                component: () => import('@/views/pages/ventas/familias/VentasReportIndex.vue')
            },
            {
                path: '/ventas-meses-familias',
                name: 'ventas-meses-familias',
                component: () => import('@/views/pages/ventas/familias/VentasMensualesFamiliasReportIndex.vue')
            },
            {
                path: '/ventas-detalles',
                name: 'ventas-detalles',
                component: () => import('@/views/pages/ventas/detalle/VentasDetallesReportIndex.vue')
            },
            {
                path: '/venta/:factura/:tipo/:sucursal',
                name: 'venta',
                component: () => import('@/views/pages/ventas/VentaShow.vue')
            },

            {
                path: 'cobro/venta/:factura/:tipo/:sucursal',
                name: 'cobros/venta',
                component: () => import('@/views/pages/ventas/CobroReportIndex.vue')
            },
            {
                path: 'plan-pago/venta/:factura/:tipo/:sucursal',
                name: 'plan-pago/venta',
                component: () => import('@/views/pages/ventas/PlanPagoReportIndex.vue')
            },

            {
                path: '/cobros/detalles',
                name: 'cobros/detalles',
                component: () => import('@/views/pages/cobros/CobrosIndex.vue')
            },
            {
                path: '/cobros/:id',
                name: 'cobro',
                component: () => import('@/views/pages/cobros/CobroShow.vue')
            },
            {
                path: '/cobro',
                name: 'cobro-insert',
                component: () => import('@/views/pages/cobros/CobroShow.vue')
            },
            {
                path: '/cobros',
                name: 'cobros',
                component: () => import('@/views/pages/ventas/CobrosReportIndex.vue')
            },

            {
                path: '/parametros-generales',
                name: 'parametros-generales',
                component: () => import('@/views/pages/parametros/ParametrosShow.vue')
            },
            {
                path: '/asientos/:anio/:empresa/:asiento',
                name: 'asientos',
                component: () => import('@/views/pages/asientos/AsientosShow.vue')
            },

            {
                path: '/estado-cuenta/proveedores',
                name: 'estado-cuenta/proveedores',
                component: () => import('@/views/pages/proveedores/EstadosCuentaReportIndex.vue')
            },
            {
                path: '/estado-cuenta/proveedor/:id',
                name: 'estado-cuenta/proveedor',
                component: () => import('@/views/pages/proveedores/EstadoCuentaReportIndex.vue')
            },
            {
                path: '/estado-cuenta/clientes',
                name: 'estado-cuenta/clientes',
                component: () => import('@/views/pages/clientes/EstadosCuentaReportIndex.vue')
            },

            {
                path: '/estado-cuenta/cliente/:id',
                name: 'estado-cuenta/cliente',
                component: () => import('@/views/pages/clientes/EstadoCuentaReportIndex.vue')
            },
            {
                path: '/resumen-ventas-estadisticas',
                name: 'resumen-ventas-estadisticas',
                component: () => import('@/views/pages/graficos/ventas/VentasMensuales.vue')
            },
            {
                path: '/resumen-compras-estadisticas',
                name: 'resumen-compras-estadisticas',
                component: () => import('@/views/pages/graficos/compras/ComprasMensuales.vue')
            },
            {
                path: '/operadores',
                name: 'operadores',
                component: () => import('@/views/pages/operadores/OperadoresIndex.vue')
            },
            {
                path: '/operador/:id',
                name: 'operador',
                component: () => import('@/views/pages/operadores/OperadorShow.vue')
            },
            {
                path: '/operador',
                name: 'operador-insert',
                component: () => import('@/views/pages/operadores/OperadorShow.vue')
            },
            {
                path: '/maquinas',
                name: 'maquinas',
                component: () => import('@/views/pages/maquinas/MaquinasIndex.vue')
            },
            {
                path: '/maquina/:id',
                name: 'maquina',
                component: () => import('@/views/pages/maquinas/MaquinaShow.vue')
            },
            {
                path: '/maquina',
                name: 'maquina-insert',
                component: () => import('@/views/pages/maquinas/MaquinaShow.vue')
            },
            {
                path: '/operaciones',
                name: 'operaciones',
                component: () => import('@/views/pages/operaciones/OperacionesIndex.vue')
            },
            {
                path: '/operacion/:id',
                name: 'operacion',
                component: () => import('@/views/pages/operaciones/OperacionShow2.vue')
            },
            {
                path: '/operacion',
                name: 'operacion-insert',
                component: () => import('@/views/pages/operaciones/OperacionShow2.vue')
            },
            {
                path: '/trabajo/extrusion/:trabajo/:item/:tipo',
                name: 'orden-extrusion-trabajo-insert',
                component: () => import('@/views/pages/operaciones/OperacionShow.vue')
            },
            {
                path: '/trabajo/corte/:trabajo/:item/:tipo',
                name: 'orden-corte-trabajo-insert',
                component: () => import('@/views/pages/operaciones/OperacionShow.vue')
            },
            {
                path: '/trabajos',
                name: 'trabajos',
                component: () => import('@/views/pages/operaciones/OrdenesTrabajoIndex.vue')
            },
            {
                path: '/trabajo/:id',
                name: 'trabajo',
                component: () => import('@/views/pages/operaciones/OrdenTrabajoShow.vue')
            },
            {
                path: '/trabajo',
                name: 'trabajo-insert',
                component: () => import('@/views/pages/operaciones/OrdenTrabajoShow.vue')
            },
            {
                path: '/depositos',
                name: 'depositos',
                component: () => import('@/views/pages/depositos/DepositosIndex.vue')
            },
            {
                path: '/deposito/:id',
                name: 'deposito',
                component: () => import('@/views/pages/depositos/DepositoShow.vue')
            },
            {
                path: '/deposito',
                name: 'deposito-insert',
                component: () => import('@/views/pages/depositos/DepositoShow.vue')
            },
            {
                path: '/auditoria/dia',
                name: 'auditoria-dia',
                component: () => import('@/views/pages/auditoria/AuditoriaDiaIndex.vue')
            }
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('@/views/pages/auth/Login.vue')
    },
    // Ruta para Not Found
    {
        path: '/:catchAll(.*)',
        component: import('@/views/pages/NotFound.vue')
    }
];

export default routes;
