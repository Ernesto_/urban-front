import { createRouter, createWebHistory } from 'vue-router';
import { storeToRefs } from 'pinia';
import routes from './routes';
import { useAuthStore } from '@/stores/auth';

const router = createRouter({
    routes,
    history: createWebHistory()
});

router.beforeEach((to, from) => {
    const store = useAuthStore();
    const { isLoggedIn } = storeToRefs(store);

    if (to.meta.auth && !isLoggedIn.value) {
        return {
            name: 'login',
            query: {
                redirect: to.fullPath
            }
        };
    } else if ((to.meta.guest || to.name == 'login') && isLoggedIn.value) {
        return { name: 'dashboard' };
    }
});

export default router;
